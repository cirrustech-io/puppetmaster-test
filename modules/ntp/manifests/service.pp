# == Class: ntp::service
class ntp::service inherits ntp {

# == Variables
$service_name = $::osfamily ? {
                   'Debian'	=>  'ntp',
                   'RedHat'	=>  'ntpd',
									 default	=>	'ntp',
                 }

# == Service Definition
 service { $service_name:
    ensure      =>  'running',
    enable      =>  'true',
    hasstatus   =>  'true',
    hasrestart  =>  'true',
    require     =>  Class['ntp::install'],
  }

}
