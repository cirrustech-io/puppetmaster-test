# == Class: ntp::params
class ntp::params {

# == Variables
$ntp_service_name = $::osfamily ? {
                    'Debian' =>  'ntp',
                    'RedHat' =>  'ntpd',
                    default  =>  'ntp',
                  }
$ntp_packages			= ['ntp', 'ntpdate']


}
