# == Class: ntp
class ntp (
	$ntp_packages			= $ntp::params::ntp_packages,
	$ntp_service_name	= $ntp::params::ntp_service_name,
) inherits ::ntp::params  {

	validate_array($ntp_packages)
	validate_string($ntp_service_name)

	contain ntp::install
	contain ntp::config
	contain ntp::service

}
