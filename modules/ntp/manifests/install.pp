# == Class: ntp::install
class ntp::install inherits ntp {

# == Variables
#$ntp_packages = ['ntp', 'ntpdate']
#$ntp_packages	=	hiera('ntp_packages')

  package { $ntp_packages:
    ensure	=> installed,
  }

}
