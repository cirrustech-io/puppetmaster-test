Facter.add(:byobu_check) do
	setcode do
		%x{STATUS=`/usr/bin/dpkg -l byobu | grep byobu | wc -l`}.chomp
		if $STATUS == 1
			%x{echo "byobu is installed"}.chmop
		end
		if $STATUS == 0
			%x{echo "byobu is not installed"}.chomp
		end
	end
end
